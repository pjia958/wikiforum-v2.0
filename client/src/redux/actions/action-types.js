export const POSTING_ARTICLE_START = 'posting_article_start';
export const POSTING_ARTICLE_END = 'posting_article_end';
export const POSTING_ARTICLE_SUCCESS = 'posting_article_success';
export const POSTING_ARTICLE_FAILURE = 'posting_article_failure';

// export const UPDATE_ARTICLE_TITLE = 'update_article_title';
// export const UPDATE_ARTICLE_CONTENT = 'update_ARTICLE_content';
// export const UPDATE_DISCUSSION_PIN_STATUS = 'update_discussion_pin_status';
// export const UPDATE_DISCUSSION_TAGS = 'update_discussion_tags';

// export const CLEAR_SUCCESS_MESSAGE = 'clear_success_message';
